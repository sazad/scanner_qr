﻿using System;

using Xamarin.Forms;

namespace Scanner_QR.View
{
    public partial class ValuePage : ContentPage
    {
        public ValuePage(int major, int minor)
        {
            InitializeComponent();
            var label = new Label();
            var layout = new StackLayout { Padding = new Thickness(5, 10) };
            this.Content = layout;

            if(major == 0 && minor == 0){
				label = new Label
				{
					Text = "No QR Code has been scanned yet",
					FontSize = 30
				};
            }
				
            else{
				label = new Label
				{
					Text = "Major: " + major + Environment.NewLine
																			+ "Minor " + minor,
					FontSize = 30
				};    
            }
            layout.Children.Add(label);
        }
    }
}
