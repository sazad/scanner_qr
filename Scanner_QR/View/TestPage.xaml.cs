﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Scanner_QR.View
{
    public partial class TestPage : ContentPage
    {

        public TestPage()
        {
            InitializeComponent();
        }
        
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            DisplayAlert("Title", "Hello World", "OK");
        }

    }
}
