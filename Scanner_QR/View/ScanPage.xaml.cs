﻿using System;
using ZXing.Net.Mobile.Forms;
using Xamarin.Forms;

namespace Scanner_QR.View
{
    public partial class ScanPage : ContentPage
    {
        ZXingScannerPage scanpage;
        Button scanButton;
        Button valueButton;
        String scanResult;
        int major;
        int minor;

        public ScanPage() : base()
        {
            scanButton = new Button
            {
                Text = "Scan Code",
                AutomationId = "scanButton",
            };

            valueButton = new Button
            {
                Text = "Major Minor Values",
                AutomationId = "valueButton",
            };

            TriggerScanpage();
            TriggerValuePage();

			var stack = new StackLayout();
			stack.Children.Add(scanButton);
            stack.Children.Add(valueButton);
			Content = stack;
        }

        private void TriggerScanpage()
        {
			scanButton.Clicked += async delegate
			{
				scanpage = new ZXingScannerPage();
				scanpage.OnScanResult += (result) =>
				{
					scanpage.IsScanning = false;

					Device.BeginInvokeOnMainThread(() =>
					{
						Navigation.PopAsync();
						scanResult = result.Text;
                        SetMajorMinor();
						DisplayAlert("","Scan successful","OK");
					});
				};
				await Navigation.PushAsync(scanpage);
			};
        }

        private void TriggerValuePage()
        {
            valueButton.Clicked += async delegate 
            {
                await Navigation.PushAsync(new ValuePage(major, minor));    
            };    
        }

        private void SetMajorMinor()
        {
            char[] array = scanResult.ToCharArray();
            string stringMajor = "";
            string stringMinor = "";
            bool isMajor = true;

            for (int i = 0; i < array.Length; i++)
            {
                if (isMajor)
                {
                    if (array[i].Equals(':'))
                    {
                        isMajor = false;
                    }
                    else
                    {
                        stringMajor += array[i];
                    }
                }
                else
                {
					stringMinor += array[i];
                }
            }

            this.major = Int32.Parse(stringMajor);
            this.minor = Int32.Parse(stringMinor);
        }
    }
}
